
## Customized validation
The files **topic.json** and **user-topic.json** are used to validate topics and kafka user files, here is the [JSON Schema](https://json-schema.org/) documentation.

To see what is validated, here's a basic yaml file:
```yaml
apiVersion: kafka.strimzi.io/v1beta1
kind: KafkaTopic
metadata:
  labels:
    strimzi.io/cluster: kafka-prod-cluster
  name: bell.specific.name
spec:
  config:
    compression.type: lz4
    min.insync.replicas: 2
    retention.bytes: 100000000
    retention.ms: 172800000
  partitions: 10
  replicas: 3
  topicName: bell.specific.name

```
First, in the json schemas, each properties is either an object, a string, an integer, etc. If the value for a string property is not a string the validation will detect it, for example if the value for the _metadata.name_ property is _1234_, the validation will fail and so will the pipeline, if a merge request was made.

1.  The properties _apiVersion, kind, metadata, spec, metadata.name, metadata.labels, metadata.labels.strimzi.io/cluster, etc..._ (see the _required_ property in the **topic.json** file for more details) are _required_, meaning that if they are not present, the validation will fail.

2.   _metadata.name_ needs to start with "bell." and cannot contain special characters such as !?}{_][

3.  The value for _labels.strimzi.io/cluster_ needs to be _kafka-prod-cluster_ or _kafka-preprod-cluster_

4.  The value for _spec.config.compression.type_ needs to be _uncompressed, zstd, lz4, snappy, gzip or producer_  

5.  The value for _spec.config.message.timestamp.type_ needs to be _kafka-preprod-cluster or kafka-prod-cluster_

6.  For properties inside of _spec.config_ that have integer as values, values have a _maximum_, a _minimum_ and a _default_ value.

and more...

To see all the requirements to create a Kafka topic, please refer to the confluence page:

# Kafka Users

For Kafka users file, the validation for the _apiVersion, kind and metadata_ properties are almost the same (except for the _owner-email_ property).

To see all the requirement to create Kafka, please refer to the confluence page and the README.md in Gitlab.



`yamlvalidate`
---

A command line tool for validating `yaml` files against a [JSON Schema](https://json-schema.org/).

This utility was originally written for verifying Kubernetes YAML files, though it is generic enough to be used for any valid YAML file and JSON schema. 

## Usage

```console
./yamlvalidate -s example/schema.json example/invalid.yaml 
```

## Examples

### Valid File
```console
$ .yamlvalidate -s example/schema.json example/valid.yaml 
2019/09/17 15:37:28 All files validated successfully!
```

### Invalid File
```console
$ .yamlvalidate -s example/schema.json example/invalid.yaml 
2019/09/17 15:38:42 error validating file example/invalid.yaml - tags.2 Invalid type. Expected: string, given: integer
2019/09/17 15:38:42 error validating file example/invalid.yaml - dimensions.height Invalid type. Expected: integer, given: string
2019/09/17 15:38:42 error validating file example/invalid.yaml - price Invalid type. Expected: number, given: string
2019/09/17 15:38:42 Validation failed
2019/09/17 15:38:42 Please fix the following files: 
2019/09/17 15:38:42 example/invalid.yaml
```

### Mixed Files
```console
$ .yamlvalidate -s example/schema.json example/valid.yaml example/invalid.yaml
2019/09/17 15:38:14 error validating file example/invalid.yaml - price Invalid type. Expected: number, given: string
2019/09/17 15:38:14 error validating file example/invalid.yaml - tags.2 Invalid type. Expected: string, given: integer
2019/09/17 15:38:14 error validating file example/invalid.yaml - dimensions.height Invalid type. Expected: integer, given: string
2019/09/17 15:38:14 Validation failed
2019/09/17 15:38:14 Please fix the following files: 
2019/09/17 15:38:14 example/invalid.yaml
```

## Validating Kubernetes Schemas

By [extending the JSON Schema](https://json-schema.org/understanding-json-schema/structuring.html) of the Kubernetes API, we are able to write our own typesafe validation pipelines on top of the already powerful OpenAPI spec for all Kubernetes objects. Business domain specific rules can enforce constraints on Kubernetes files, such as resource limits or whitelisted registry domains without needing to migrate all of your kubernetes files from `yaml` to a protocol-buffer based framework like [isopod](https://github.com/cruise-automation/isopod) or [skycfg](https://github.com/stripe/skycfg).

For example: the following rule can be used to check that all [`Quantity`](https://github.com/kubernetes/apimachinery/blob/master/pkg/api/resource/quantity.go) declarations use no more than a single (numeric) virtual core. 

```json
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "https://brendanjryan.com/limited_quantity.json",
  "allOf": [
    { 
      "$ref": "https://kubernetesjsonschema.dev/master/_definitions.json#/definitions/io.k8s.apimachinery.pkg.api.resource.Quantity"
    },
    { 
      "type": "number",
      "minimum": 0,
      "exclusiveMaximum": 1
    }
  ]
}
```

