#!/bin/sh
#file="/Users/sami.yahia/k8s-ci-cd/yamlValidation-test/kafka-topic.yaml"
file=$1
numApi=$(grep apiVersion: -c $file)

set -e
if [ $1 = "yamlValidation-test/kafka-topic.yaml" ];
then
    printf "Each block consists of kafka configurations separated by 3 dashes ('---') \n"
    for (( i=1; i<=$numApi; i++))
    do
        printf "This is block number $i: \n"
        awk -v i=$i '/^apiVersion:/,/^---/{if(++m==1)n++;if(n==i)print;if(/^---/)m=0}' $file > ./block.yaml
        go run "yamlValidation-test/yamlvalidate-master/main.go" -s "yamlValidation-test/yamlvalidate-master/topic.json" ./block.yaml
        rm block.yaml
    done
else
    go run "yamlValidation-test/yamlvalidate-master/main.go" -s "yamlValidation-test/yamlvalidate-master/user-topic.json" "yamlValidation-test/user-topic.yaml"
fi
